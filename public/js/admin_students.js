
function view() {
  var numItems = $('#students_table input:checked').length;
  if(numItems == 1) {
    $('#students_table input:checked').each(function(i,obj) {
        var apid = $(obj).val()
        var newtab = window.open( '', '_blank' );
        newtab.location = getUrl() + '/main/view_student/' + apid;
    });
  } else {
    swal({
      title: 'Oops!',
      type: 'error',
      html:
        "Please select a student.",
      confirmButtonText: 'Ok',
      showCancelButton: false,
      allowOutsideClick: false
    }).catch(swal.noop);
  }
  /*$('#dashboard_table input:checked').each(function(i,obj) {
      var apid = $(obj).val()
  });*/
}

function previewFile() {
  var preview = document.getElementById('placeholder');
  var other_image = document.getElementById('other_image');
  var file    = document.querySelector('input[type=file]').files[0];
  var reader  = new FileReader();

  reader.addEventListener("load", function () {
    preview.src = reader.result;
    other_image.src = reader.result;
  }, false);

  if (file) {
    reader.readAsDataURL(file);
  }

  return file;
}

function getBase64Image(img) {
  var canvas = document.createElement("canvas");
  canvas.width = img.width;
  canvas.height = img.height;
  var ctx = canvas.getContext("2d");
  ctx.drawImage(img, 0, 0);
  var dataURL = canvas.toDataURL("image/png");
  return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
}

function create() {
  var div = "<div class='form-group'>";
  var divclose = "</div>";
  var row = "<div class='row'>";
  var name = "<div class='col-md-8'>" + div + "<label for='name' style='text-align:left;'>Name</label><input id='name' class='form-control input-lg' placeholder='Enter full name...'>" + divclose;
  var age = div + "<label for='age' style='text-align:left;'>Age</label><input id='age' class='form-control input-lg' placeholder='Enter age...'>" + divclose;
  var date = div + "<label for='date' style='text-align:left;'>Date of Birth</label><input id='date' class='form-control input-lg' type='date' placeholder='Enter date...'>" + divclose + divclose;
  getData(getUrl() + "/admin_api/get_all_classes", function(classes) {
    if(classes.response == 1) {
      swal({
        title: 'Oops!',
        type: 'error',
        html:
          classes.message,
        confirmButtonText: 'Ok',
        showCancelButton: false,
        allowOutsideClick: false
      }).catch(swal.noop);
    } else {
      var file_upload = "<div class='col-md-4'>" + div + "<label for='profile_picture' style='text-align:left;'>Profile Picture</label><img class='image_upload' width='250' height='250' id='placeholder' src='http://placehold.it/250x250&text=Click'>"+
      "<input onchange='previewFile()' style='display:none;' id='profile_picture' type='file' ><br>" + divclose + divclose;
      var img = "<img src='' id='other_image' style='display:none'/>"
      var select_classes = div + "<label for='classes' style='text-align:left;'>Classes</label><input id='classes' placeholder='Enter class...'>" + divclose;

      var ar_subjects = []
      var subs = []
      var order = []
      var ar_years = []
      for(var i = 0; i < classes.length; i++) {
        ar_subjects.push({id: classes[i]._id, subject: classes[i].subject, name: classes[i].name})
        subs.push({id: classes[i].subject, name: classes[i].subject});
        order.push(classes[i].subject);
      }
      swal({
        title: 'Create Student',
        type: 'info',
        width:'80%',
        html:
          row +
          file_upload +
          name +
          date +
          divclose +
          select_classes +
          img,
        preConfirm: function () {
          return new Promise(function (resolve, reject) {
            if($('#name').val() != '' && $('#date').val() != '' && $('#classes').val() != '') {
              if($('#profile_picture').val() != '') {
                var myBase64Data = getBase64Image(document.getElementById('other_image'));
                var byteData = [];
                for (var i = 0; i < myBase64Data.length; i++){
                    byteData.push(myBase64Data.charCodeAt(i));
                }
                resolve([
                  $('#name').val(),
                  $('#date').val(),
                  $('#classes').val(),
                  byteData
                ])
              } else {
                resolve([
                  $('#name').val(),
                  $('#date').val(),
                  $('#classes').val()
                ])
              }
           } else {
             reject('Please fill in all fields!')
           }
          })
        },
        onOpen: function () {
          $('#placeholder').click(function() {
            $('#profile_picture').trigger('click');
          });

          $('#name').focus()
          $('#classes').selectize({
            plugins: ['remove_button', 'optgroup_columns'],
            persist: false,
            maxItems: null,
            valueField: 'id',
            labelField: 'name',
            searchField: ['name', 'subject'],
            options: ar_subjects,
            optgroups: subs,
            optgroupField: 'subject',
            optgroupLabelField: 'name',
            optgroupValueField: 'id',
            optgroupOrder: order,
            render: {
             item: function(item, escape) {
                 return '<div>' +
                     (item.subject ? '<span class="name">' + escape(item.subject) + '</span>' : '') +
                     (item.name ? '<span class="email">' + escape(item.name) + '</span>' : '') +
                 '</div>';
             },
             option: function(item, escape) {
                 var label = item.name || item.subject;
                 var caption = item.name ? item.subject : null;
                 return '<div>' +
                     '<div class="label" style="padding:0px;margin:0px">' + escape(label) + '</div>' +
                     (caption ? '<div class="caption">' + escape(caption) + '</div>' : '') +
                 '</div>';
             }
           },
          });
        }
      }).then(function (result) {
        var data = {}
        if(result.length != 5) {
          data = {
            name: result[0],
            dob: result[1],
            classes: result[2]
          }
        } else {
          data = {
            name: result[0],
            dob: result[1],
            classes: result[2],
            file: result[3]
          }
        }
        postData(getUrl() + "/admin_api/create_student", data, function(response) {
          if(response != 413) {
            if(response.response != 1) {
              swal({
                title: 'Success!',
                type: 'success',
                html:
                  "Student: " + account.name + " has successfully been edited!",
                confirmButtonText: 'Ok',
                showCancelButton: false,
                allowOutsideClick: false
              }).then(function() {
                location.reload();
              }).catch(swal.noop);
            } else {
              swal({
                title: 'Oops!',
                type: 'error',
                html:
                  response.message,
                confirmButtonText: 'Ok',
                showCancelButton: false,
                allowOutsideClick: false
              }).catch(swal.noop);
            }
          } else {
            swal({
              title: 'Oops!',
              type: 'error',
              html:
                "Please upload a smaller image!",
              confirmButtonText: 'Ok',
              showCancelButton: false,
              allowOutsideClick: false
            }).catch(swal.noop);
          }
        });
      }).catch(swal.noop)
    }
  });
}

function edit() {
  var numItems = $('#students_table input:checked').length;
  if(numItems == 1) {
    var apid = $('#students_table input:checked').first().val()
    getData(getUrl() + "/api/get_student_data/" + apid, function(response) {
      getData(getUrl() + "/admin_api/get_all_classes", function(classes) {
        if(classes.response == 1) {
          swal({
            title: 'Oops!',
            type: 'error',
            html:
              classes.message,
            confirmButtonText: 'Ok',
            showCancelButton: false,
            allowOutsideClick: false
          }).catch(swal.noop);
        } else {
          if(response.response != 1) {
            var account = response;
            getData(getUrl() + "/api/get_groups/", function(groups) {
              if(groups.response != 1) {
                var div = "<div class='form-group'>";
                var divclose = "</div>";
                var colmd6 = "<div class='col-md-6'>";
                var row = "<div class='row'>";
                var name = div + "<label for='name' style='text-align:left;'>Name</label><input id='name' value='"+response.name+"' class='form-control input-lg' placeholder='Enter full name...'>" + divclose;
                var select_classes = div + "<label for='classes' style='text-align:left;'>Classes</label><input id='classes' placeholder='Enter class...'>" + divclose;
                var file_upload = div + "<label for='profile_picture' style='text-align:left;'>Profile Picture</label><img class='image_upload' width='250' height='250' id='placeholder' src='"+response.picture_url+"'>"+
                "<input onchange='previewFile()' style='display:none;' id='profile_picture' type='file' ><br>" + divclose + divclose;
                var img = "<img src='"+response.picture_url+"' id='other_image' style='display:none'/>"
                var date = div + "<label for='date' style='text-align:left;'>Date of Birth</label><input id='date' value='"+response.dob+"'class='form-control input-lg' type='date' placeholder='Enter date...'>" + divclose;
                var select_classes = div + "<label for='classes' style='text-align:left;'>Classes</label><input id='classes' value='"+response.classes.join()+"' placeholder='Enter class...'>" + divclose;
                var ar_subjects = []
                var subs = []
                var order = []
                var ar_years = []
                for(var i = 0; i < classes.length; i++) {
                  ar_subjects.push({id: classes[i]._id, subject: classes[i].subject, name: classes[i].name})
                  subs.push({id: classes[i].subject, name: classes[i].subject});
                  order.push(classes[i].subject);
                }
                swal({
                  title: 'Student: ' + response.name,
                  type: 'info',
                  width:'70%',
                  html:
                    row +
                    colmd6 +
                    file_upload +
                    colmd6 +
                    name +
                    date +
                    divclose +
                    divclose +
                    select_classes +
                    img,
                  preConfirm: function () {
                    return new Promise(function (resolve, reject) {
                      if($('#name').val() != '' && $('#age').val() != '' && $('#date').val() != '' && $('#classes').val() != '') {
                        if($('#profile_picture').val() != '') {
                          var myBase64Data = getBase64Image(document.getElementById('other_image'));
                          resolve([
                            $('#name').val(),
                            $('#date').val(),
                            $('#classes').val(),
                            myBase64Data
                          ])
                        } else {
                          resolve([
                            $('#name').val(),
                            $('#date').val(),
                            $('#classes').val()
                          ])
                        }
                     } else {
                       reject('Please fill in all fields!')
                     }
                    })
                  },
                  onOpen: function() {
                    $('#placeholder').click(function() {
                      $('#profile_picture').trigger('click');
                    });

                    $('#classes').selectize({
                      plugins: ['remove_button', 'optgroup_columns'],
                      persist: false,
                      maxItems: null,
                      valueField: 'id',
                      labelField: 'name',
                      searchField: ['name', 'subject'],
                      options: ar_subjects,
                      optgroups: subs,
                      optgroupField: 'subject',
                      optgroupLabelField: 'name',
                      optgroupValueField: 'id',
                      optgroupOrder: order,
                      render: {
                       item: function(item, escape) {
                           return '<div>' +
                               (item.subject ? '<span class="name">' + escape(item.subject) + '</span>' : '') +
                               (item.name ? '<span class="email">' + escape(item.name) + '</span>' : '') +
                           '</div>';
                       },
                       option: function(item, escape) {
                           var label = item.name || item.subject;
                           var caption = item.name ? item.subject : null;
                           return '<div>' +
                               '<div class="label" style="padding:0px;margin:0px">' + escape(label) + '</div>' +
                               (caption ? '<div class="caption">' + escape(caption) + '</div>' : '') +
                           '</div>';
                       }
                     },
                    });
                  },
                }).then(function (result) {
                  var data = {}
                  if(result.length != 4) {
                    data = {
                      name: result[0],
                      dob: result[1],
                      classes: result[2],
                      _id: account._id
                    }
                  } else {
                    data = {
                      name: result[0],
                      dob: result[1],
                      classes: result[2],
                      file: result[3],
                      _id: account._id
                    }
                  }
                  postData(getUrl() + "/admin_api/edit_student", data, function(response) {
                    if(response != 413) {
                      if(response.response != 1) {
                        swal({
                          title: 'Success!',
                          type: 'success',
                          html:
                            "Student: " + account.name + " has successfully been edited!",
                          confirmButtonText: 'Ok',
                          showCancelButton: false,
                          allowOutsideClick: false
                        }).then(function() {
                          location.reload();
                        }).catch(swal.noop);
                      } else {
                        swal({
                          title: 'Oops!',
                          type: 'error',
                          html:
                            response.message,
                          confirmButtonText: 'Ok',
                          showCancelButton: false,
                          allowOutsideClick: false
                        }).catch(swal.noop);
                      }
                    } else {
                      swal({
                        title: 'Oops!',
                        type: 'error',
                        html:
                          "Please upload an image smaller than 700kb / 0.7mb!",
                        confirmButtonText: 'Ok',
                        showCancelButton: false,
                        allowOutsideClick: false
                      }).catch(swal.noop);
                    }
                  });
                }).catch(swal.noop)
              } else {
                swal({
                  title: 'Oops!',
                  type: 'error',
                  html:
                    groups.message,
                  confirmButtonText: 'Ok',
                  showCancelButton: false,
                  allowOutsideClick: false
                }).catch(swal.noop);
              }
            });
          } else {
            swal({
              title: 'Oops!',
              type: 'error',
              html:
                response.message,
              confirmButtonText: 'Ok',
              showCancelButton: false,
              allowOutsideClick: false
            }).catch(swal.noop);
          }
        }
      })
    })
  } else {
    swal({
      title: 'Oops!',
      type: 'error',
      html:
        "Please select an account.",
      confirmButtonText: 'Ok',
      showCancelButton: false,
      allowOutsideClick: false
    }).catch(swal.noop);
  }
}

function delete_acc() {
  var numItems = $('#students_table input:checked').length;
  if(numItems >= 1) {
    swal({
      title: 'Confirm?',
      type: 'info',
      html:
        "Are you sure you want to delete " + numItems + " student(s)?",
      confirmButtonText: 'Yes',
      showCancelButton: true,
      allowOutsideClick: false
    }).then(function () {
      $('#students_table input:checked').each(function(i,obj) {
          var apid = $(obj).val()
          getData(getUrl() + "/admin_api/delete_student/" + apid, function(response) {
            if(response.response == 1) {
              swal({
                title: 'Error',
                text: response.message,
                type: 'error',
                confirmButtonText: 'Oops?',
                showCancelButton: false,
                allowOutsideClick: false
              }).catch(swal.noop);
            } else {
              if((i+1) == numItems) {
                location.reload();
              }
            }
          });
      });
    }).catch(swal.noop);
  } else {
    swal({
      title: 'Oops!',
      type: 'error',
      html:
        "Please select a student.",
      confirmButtonText: 'Ok',
      showCancelButton: false,
      allowOutsideClick: false
    }).catch(swal.noop);
  }
}
$(document).ready(function () {
  $('.account_group').each(function(i, obj) {
    var id = $(obj).text();
    getData(getUrl() + '/api/get_group_by_uuid/' + id, function(callback) {
      if(callback.response != 1) {
        $(obj).css('text-align', 'center');
        $(obj).css('color', '#fff');
        $(obj).css('padding', '10px');
        $(obj).css('background', callback.color);
        $(obj).text(callback.group_name);
      }
    });
  });

  $('.checkAll').on('click', function () {
    $(this).closest('table').find('tbody :checkbox')
      .prop('checked', this.checked)
      .closest('tr').toggleClass('selected', this.checked);
  });

  $('tbody :checkbox').on('click', function () {
    $(this).closest('tr').toggleClass('selected', this.checked);
    $(this).closest('table').find('.checkAll').prop('checked', ($(this).closest('table').find('tbody :checkbox:checked').length == $(this).closest('table').find('tbody :checkbox').length)); //Tira / coloca a seleção no .checkAll
  });
});

$('.avatar-student-year').each(function(i,obj) {
  uuid = children[i]._id;
  getData(getUrl() + "/api/get_year_data/"+uuid, function(result) {
    $(obj).text(result.name);
  });
});
