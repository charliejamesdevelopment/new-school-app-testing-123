var express = require('express');
var router = express.Router();
var utils = require('../utils/utils');


router.get('/',function(req,res){
  utils.isLoggedIn(req,function(call) {
    if(call == true) {
      res.redirect('/main/dashboard/false');
    } else {
      res.status(200)
      res.render('auth/forgot_password_reset.ejs')
    }
  });
});

module.exports = router;
