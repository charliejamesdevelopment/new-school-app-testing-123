var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var reportSchema = new Schema({
  student: { type: String, required: true },
  class: { type: String, required: true },
  term: { type: Number, required: true },
  description: { type: String, required: true },
  grade: { type: String, required: true },
  target_grade: { type: String, required: true },
  created_at: Date,
  updated_at: Date
});

reportSchema.pre('save', function(next) {

  this.active = 0;

  // get the current date
  var currentDate = new Date();

  // change the updated_at field to current date
  this.updated_at = currentDate;

  // if created_at doesn't exist, add to that field
  if (!this.created_at)
    this.created_at = currentDate;

  next();
});
reportSchema.set('collection', 'reports');

var User = mongoose.model('Report', reportSchema);

// make this available to our users in our Node applications
module.exports = User;
