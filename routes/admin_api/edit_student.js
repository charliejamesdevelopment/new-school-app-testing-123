var express = require('express');
var router = express.Router();
var utils = require('../utils/utils');
var bcrypt = require('bcrypt-nodejs');
var fs = require('fs');
var path = require('path');
var config = require('../../config.js');

router.post('/edit_student',function(req,res){
    if(req.body && req.body.name && req.body.dob && req.body.classes && req.body._id) {
      if(req.body.file) {
        var file = req.body.file;
        file = file.replace(/^data:image\/jpg;base64,/,"")
        file = file.replace(/^data:image\/png;base64,/,"")
        fs.writeFile(path.join(__dirname, '../../public/images/' + req.body.name.replace(" ", "_") + ".jpg"), file, 'base64', function(err) {
          if(err) {
            res.send({"response" : 1, "message" : "Something went wrong!"});
          } else {
            var data = {
              name: req.body.name,
              dob: req.body.dob,
              classes: req.body.classes,
              picture_url: config.website.url + "/public/images/" + req.body.name.replace(" ", "_") + ".jpg",
              _id: req.body._id
            }
            utils.editStudent(data, function(callback) {
              if(callback == 1) {
                res.send({"response" : 1, "message" : "Something went wrong!"});
              } else {
                res.send({"response" : 0});
              }
            });
          }
        });
      } else {
        var data = {
          name: req.body.name,
          dob: req.body.dob,
          classes: req.body.classes,
          picture_url: "",
          _id: req.body._id
        }
        utils.editStudent(data, function(callback) {
          if(callback == 1) {
            res.send({"response" : 1, "message" : "Something went wrong!"});
          } else {
            res.send({"response" : 0});
          }
        });
      }
    } else {
      res.send({"response" : 1, "message" : "Invalid body!"});
    }
});

module.exports = router;
