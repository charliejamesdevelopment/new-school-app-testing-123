var express = require('express');
var router = express.Router();
var utils = require('../utils/utils');
var User = require('../models/user');
var bcrypt = require('bcrypt-nodejs');

router.post('/',function(req,res){
  if(!req.session.uuid) {
    code = req.body.authentication_token;
    name = req.body.name;
    email = req.body.email;
    password = req.body.password;
    if(name == "" || code == "" || email == "" || password == "") {
      req.flash('message', 'Please fill all fields in to sign-up correctly.')
      req.flash('message_type', 'error')
      res.redirect('/auth/create_account');
    } else {
      utils.checkEmailTaken(email, function(result) {
        if(result == true) {
          req.flash('message', 'Email has already been taken!')
          req.flash('message_type', 'error')
          res.redirect('/auth/create_account');
        } else {
          var hash = bcrypt.hashSync(password);
          utils.getAuthTokenData(code, function(result) {
            if(result.group == 0) {
              if(result != undefined && result != false) {
                var newUser = User({
                  name: name,
                  email: email,
                  password: password,
                  group: result.group,
                  students: result.students,
                  bio: "",
                  picture_url: ""
                }).save(function(err) {
                  if (err) {
                    req.flash('message', 'Something went wrong!');
                    req.flash('message_type', 'error');
                    res.redirect('/auth/create_account');
                    throw err;
                  }
                  res.redirect('/');
                  console.log('User created!');
                });
              } else {
                req.flash('message', 'Invalid authentication token.')
                req.flash('message_type', 'error')
                res.redirect('/auth/create_account');
              }
            } else {
              req.flash('message', 'We have not made an account creater for teachers yet.')
              req.flash('message_type', 'error')
              res.redirect('/auth/create_account');
            }
          });

        }
      });
    }
  } else {
    res.redirect("/main/dashboard")
  }
});

module.exports = router;
